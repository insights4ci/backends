#!/usr/bin/env python3
from datetime import timedelta, datetime

import logging
import requests

from insights4ci.datasources.common import (GenericDataSource, GenericPipeline,
                                            GenericJob, GenericTestResult,
                                            GenericRunner)


LOG = logging.getLogger('insights4ci.datasources.github')

GITHUB_ENDPOINT = 'https://api.github.com/repos/'

DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

def fetch_request(namespace, url_suffix):
    url = f"{GITHUB_ENDPOINT}{namespace}/{url_suffix}"

    headers = {
            "Accept": "application/vnd.github+json"
        }
    params = {"per_page": 100}

    while url:
        response = requests.get(url, headers=headers, params=params)

        if response.status_code == 200:
            results = response.json()
            results.pop("total_count")
            _, results = results.popitem()
            for result in results:
                yield result
            link_header = response.headers.get("Link")
            if link_header:
                links = dict(
                    [(rel[6:-1], url[url.index("<")+1:-1]) for url, rel in
                     [link.split(";") for link in link_header.split(",")]])
                url = links.get("next")
            else:
                url = None
        else:
            print(f"Error getting data: {response.json()['message']}")
            break


class DataSource(GenericDataSource):

    @staticmethod
    def fetch_pipelines_since(namespace, date_from=datetime.now() - timedelta(days=30)):
        LOG.info(f"Fetching pipelines for project {namespace}...")
        result = []
        for pipeline in fetch_request(namespace, "actions/runs"):
            pipeline_date = datetime.strptime(pipeline.get("created_at"), DATE_FORMAT)
            if pipeline_date <= date_from:
                break
            if pipeline["conclusion"] in ['running', 'skipped', 'canceled']:
                continue
            result.append(Pipeline(pipeline, namespace))
        return result


class Pipeline(GenericPipeline):

    def __init__(self, data, namespace):
        super().__init__(data)
        self._namespace = namespace

    @property
    def external_id(self):
        return self.data.get("id")

    @property
    def status(self):
        return self.data.get("conclusion")

    @property
    def sha(self):
        return self.data.get("head_sha")

    @property
    def ref(self):
        return self.data.get("head_branch")

    @property
    def created_at(self):
        return self.data.get("created_at")

    @property
    def started_at(self):
        return self.data.get("run_started_at")

    @property
    def ended_at(self):
        return self.data.get("updated_at")

    @property
    def external_url(self):
        return self.data.get("html_url")

    @property
    def jobs(self):
        LOG.info(f"Fetching jobs for pipeline {self.external_id}...")
        if not self._jobs:
            self._jobs = [Job(job, Runner(job), self)
                         for job in fetch_request(self._namespace,
                                                  f"actions/runs/{self.external_id}/jobs")]
        return self._jobs


class Job(GenericJob):
    @property
    def external_id(self):
        return self.data.get('id')

    @property
    def name(self):
        return self.data.get('name')

    @property
    def status(self):
        return self.data.get('conclusion')

    @property
    def stage(self):
        return '<unknown>'

    @property
    def external_url(self):
        return self.data.get('html_url')

    @property
    def started_at(self):
        return self.data.get('started_at')

    @property
    def created_at(self):
        return self.data.get('started_at')

    @property
    def ended_at(self):
        return self.data.get('completed_at')

    @property
    def test_results(self):
        LOG.info(f"Fetching test runs for job {self.external_id}...")
        result = []
        # fake test results until the JUnit datasource will be implemented.
        for i in range(1):
            result.append(TestResult({"name": "test", "classname": "class_name", "status": "success"}, self))
        return result


class Runner(GenericRunner):
    @property
    def external_id(self):
        return self.data.get('runner_id')

    @property
    def name(self):
        return self.data.get('runner_name')

    @property
    def owner(self):
        return "github"

    @property
    def architecture(self):
        # TODO: Fix this for custom runners
        return "x86_64"

    @property
    def platform(self):
        # TODO: Fix this for custom runners
        return "Linux"

    @property
    def tags(self):
        self.data.get('labels')


class TestResult(GenericTestResult):
    @property
    def name(self):
        return self.data.get('name')

    @property
    def class_name(self):
        return self.data.get('classname')

    @property
    def status(self):
        return self.data.get('status')

    @property
    def execution_time(self):
        return self.data.get('execution_time')
